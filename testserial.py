#!/usr/bin/python
import serial
import time
import sys
class SerialTest:
    def __init__(self):
        self.verbose = False
        self.success = 0
        self.errors = 0

    def setup(self):
        try:
            self.serial = serial.Serial(sys.argv[1], baudrate = 230400)
        except:
            print("Could not open %s" % (sys.argv[1]))
            return False

        try:
            self.loops = int(sys.argv[2])
        except:
            self.loops = 1024

        print("I will read %d frames" % self.loops)
        return True

    def run(self):
        byte = 0
        while byte != '\xFF':
            byte = self.serial.read(1)

        tim = time.time()
        print("Syncronized...")
        ascii = self.serial.read(32)
        loops = self.loops
        count = 0
        while loops > 0:
            if ascii == "000102030405060708090A0B0C0D0E0F":
                self.success=self.success+1
            else:
                self.errors=self.errors+1
                print ascii

            count = count+1
            if (count & 0xF) != 0xF:
                ascii = str(self.serial.read(32))
            else:
                byte = self.serial.read(1)
                print(str(byte))

            if self.verbose:
                print(ascii)

            loops = loops-1

        self.elapsed = time.time() - tim

    def result(self):
        print("============ Result =============")
        print("Elapsed time %f" % self.elapsed)
        print("Errors: %d" % self.errors)
        if self.errors > 0:
            print("%d Percent fails on %d frames" % ((self.errors / (self.loops*1.0))*100, self.loops))


test = SerialTest()
test.setup()
test.run()
test.result()
